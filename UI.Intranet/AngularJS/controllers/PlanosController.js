﻿app.factory('PlanosFactory', function () {

    var lblPlanosIU = '';
    var acciones = '';
    var planosFactory = {};

    planosFactory.lblPlanosIU = lblPlanosIU;
    planosFactory.acciones = acciones;
    planosFactory.data = {
        ID: '',
        Nombre: '',
        ID_Estado: ''
    };

    return planosFactory;
});

app.controller('PlanosController', function ($scope, $location, $route, $routeParams, $timeout, $compile, $http, $translate,PlanosFactory, CommonDataFactory) {

    $scope.$on("languageChanged", function (evt, data) {

        var dTable = $("#PlanosTable").DataTable();

        $(dTable.column(1).header()).text($translate.instant("index.titleName"));
        $(dTable.column(2).header()).text($translate.instant("index.titleStatus"));
        $(dTable.column(3).header()).text($translate.instant("index.titleActions"));

        var oSettings = dTable.settings();

        oSettings[0].oLanguage = currentLang;

        dTable.draw();
    });

    $scope.go = function (url, acciones, data) {

        $location.path("/" + url);

        PlanosFactory.lblPlanosIU = acciones === 'edit' ?
            $translate.instant("index.planesEdit") :
            $translate.instant("index.planesCreate");
        PlanosFactory.acciones = acciones;

        if (acciones === 'insert') {

            PlanosFactory.data = {
                ID: null,
                Nombre: null,
                ID_Estado: null
            };
        }
        else {

            PlanosFactory.data = {
                ID: data.ID,
                Nombre: data.Nombre,
                ID_Estado: data.ID_Estado
            };
        }
    };

    $scope.Inicializar = function () {

        $('#cover-spin').show(0);

        $('#PlanosTable').DataTable
            ({
                ajax:
                    {
                        url: urlBase + "blueprints",
                        type: "GET",
                        dataType: 'json',
                        contentType: "application/json;charset=utf8'",
                        dataSrc: function (json) {

                            $('#cover-spin').hide(0);
                            if (json.length !== null) {
                                return json;
                            }
                            else {
                                return false;
                            }
                        }
                    },
                dom: 'Bfrtip',
                stateSave: true,
                stateSaveCallback: function (settings, data) {
                    localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data))
                },
                stateLoadCallback: function (settings) {
                    return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance))
                },
                scrollY: "480px",
                pageLength: 10,
                columns: [
                    {
                        data: 'ID',
                        title: 'ID'
                    },
                    {
                        data: 'Nombre',
                        title: $translate.instant("index.titleName")
                    },
                    {
                        data: 'ID_Estado',
                        render: function (data, type, row, meta) {

                            return row.ID_Estado === 1 ?
                                $translate.instant("index.active") :
                                $translate.instant("index.inactive");
                        },
                        title: $translate.instant("index.titleStatus")
                    },
                    {
                        data: function (row, type, set, meta) {

                            var buttonView = "<a role='button' ng-click='go(\"planosView\",\"view\"," + JSON.stringify(row) + ")'><span title='" + $translate.instant("index.planeView") + "' class='fa fa-eye'></a>&nbsp&nbsp";
                            var buttonEdit = "<a role='button' ng-click='go(\"planosEdit\",\"edit\"," + JSON.stringify(row) + ")'><span title='" + $translate.instant("index.planeEdit") + "' class='glyphicon glyphicon-pencil'></span></a>&nbsp&nbsp";
                            var buttonDelete = "<a role='button' ng-click='PlanosDelete(" + row.ID + ")'><span title='" + $translate.instant("index.planeDelete") + "' class='glyphicon glyphicon-trash'></span></a>";

                            return buttonView + buttonEdit + buttonDelete;
                        },
                        title: $translate.instant("index.titleActions"),
                        orderable: false,
                        className: 'text-left',
                        width: '90px'
                    }
                ],
                createdRow: function (row, data, index) {
                    $compile(row)($scope);
                },
                buttons: {
                    buttons:
                        [
                            'pageLength',
                            {
                                extend: 'pdfHtml5',
                                text: 'PDF',
                                pageSize: 'A4',
                                orientation: 'portrait',
                                exportOptions: { columns: ':visible' },
                                customize: function (doc) {

                                    //APLICO ESTILOS AL PDF
                                    doc.styles["detalleHeaderStyle"] = {
                                        fontSize: 9,
                                        bold: true,
                                        fillColor: "#FAFAFA",
                                        color: "black",
                                        alignment: "center"
                                    };
                                    doc.styles["detalleInfoStyle"] = {
                                        fontSize: 9,
                                        bold: false,
                                        fillColor: "#FAFAFA",
                                        color: "black"
                                    };

                                    doc.styles["tableBodyEven"] = {
                                        fillColor: '#FAFAFA'
                                    };
                                    doc.styles["tableBodyOdd"] = {
                                        fillColor: '#D8D8D8'
                                    };

                                    //Inserto la cabecera
                                    doc.content.splice(0, 0, {
                                        layout: "noBorders",
                                        table: {
                                            widths: ['auto', '98%'],
                                            body: [
                                                [{
                                                    image: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAAQABAAD/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/hAytodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6M0FDOEY0MzJGQzExMTFFMzgxOEJGNDU2NzM4RTY1ODEiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6M0FDOEY0MzNGQzExMTFFMzgxOEJGNDU2NzM4RTY1ODEiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDozQUM4RjQzMEZDMTExMUUzODE4QkY0NTY3MzhFNjU4MSIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDozQUM4RjQzMUZDMTExMUUzODE4QkY0NTY3MzhFNjU4MSIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Pv/bAEMABQMEBAQDBQQEBAUFBQYHDAgHBwcHDwsLCQwRDxISEQ8RERMWHBcTFBoVEREYIRgaHR0fHx8TFyIkIh4kHB4fHv/bAEMBBQUFBwYHDggIDh4UERQeHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHv/AABEIAEsAfQMBEQACEQEDEQH/xAAfAAABBQEBAQEBAQAAAAAAAAAAAQIDBAUGBwgJCgv/xAC1EAACAQMDAgQDBQUEBAAAAX0BAgMABBEFEiExQQYTUWEHInEUMoGRoQgjQrHBFVLR8CQzYnKCCQoWFxgZGiUmJygpKjQ1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4eLj5OXm5+jp6vHy8/T19vf4+fr/xAAfAQADAQEBAQEBAQEBAAAAAAAAAQIDBAUGBwgJCgv/xAC1EQACAQIEBAMEBwUEBAABAncAAQIDEQQFITEGEkFRB2FxEyIygQgUQpGhscEJIzNS8BVictEKFiQ04SXxFxgZGiYnKCkqNTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqCg4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2dri4+Tl5ufo6ery8/T19vf4+fr/2gAMAwEAAhEDEQA/APsugAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgDO13XdF0K0N3rWq2Omwf89LqdYlP0LEZq4U51HaCuTOcYK8nY871P8AaD+FtpN5Ftr02qTD+DT7KWf9QuP1rtjlmJkruNvVnNLHUU7XOZ8aftMaLpGkLc6X4S8RzTvIEj/tG0NnA46thzuJOOwFb0conOVpSXy1MK2YxhG6izWtf2i/Bz2sNzdaD4vs4ZUDiWTSGaMgjOQyk5HvWbyqreykn8zRY+DV2mdB4e+N/wALdbmSC28X2NvOxx5V6GtmB9P3gA/WsamXYmGrh92ppDGUZ7SPQbaeC5gSe3mjmicZR42DKw9QRwa42mnZnSmnqiSkMKACgAoAKACgAoAKACgDwr9qb4w6h4BitPDvhxUTWr+EzvdOoYW0WSoKqeC5IbGeABnByK9bLMBHENznsvxPOx+MdFcsd2U/gh8JvC3ifwxpvjzxneS+M9Y1OITtJfTtLDASf9WEJwSvQ7sjIOAKrG42pSm6NJcqXYWFw0JwVSb5mz3XS9J0vSoRDpmnWdjEOAlvAsaj8FAryZTlPWTuehGEY7Kx8l/t0+IPtPjDQ/DcUmUsLRrqVQf+WkrYGfoqf+PV9FklK1OU310+48TNql5KB7D+yJr/APbnwW063kfdPpUslg/POFO5P/HHUfhXm5rS9niW++p35fU56K8j0HxJ4M8J+JIHi13w7peoKwwWntlZh9Gxkfga46eIq0vgk0dU6NOfxI+aPjTp0/wD17StX+HHiO5sbXUZH87Q7iYzQ4XB3BGPKHO3J+YHo3p7mDkswi4143t1PJxSeDkpUnv0PoX4PeOLf4heA7HxJDb/AGaWUtFcwZyIpkOGAPcdCD6EV42Lw7w9VwPTw9ZVqamdhXMbhQAUAFABQAUAFABQB43+0n8HG+JNlbaro9zFba9YRtHH5xIjuIid3lsf4SDkhvcg9cj08ux/1ZuMvhZwY3B+3V1uj5Ztb74r/BvU3hVtX8Pbny8cqb7Wc+vOY3+o59699xwuNjfSX5/5njJ4jCu2qO/0T9q3xtbRKmp6Fomo4/5aR+ZAx/IsP0rjnktF/DJo6YZrUXxI8g+J3i668deONR8UXkAtnvGXbAr7xEioFVQcDPA9B1r08NQVCkqa6HBiKzrTc2dZ8EPjHqXwutNWtbTSINTj1B45As07RiJ1BBPAOcggdvuiubG4COKabdrG+Exrw6ate5v+Jf2nviRqcbRaeNJ0RGGA1vbmST/vqQkf+O1jTyfDx1ld/wBeRrPM60vh0MDwn8NPif8AFfWxql1DfvHOR52r6szKm3/ZLcvjsqDH0rarjMNhI8q+5GdPDV8TLmf3s+2Phj4N03wF4MsfDWmO8sduC0kzjDTSMcu5HbJ7dhgdq+WxOIliKjqSPoaFGNGCgjpqwNQoAKACgAoAKACgAoAKAIrq3t7qB4LmGOaJxho5FDKw9weDTTad0JpPRnn/AIv+DXw91uwvDH4P0SHUJIXEEyQGELIVO1m8vGQDgmuylj69Nr33b+u5zVMHSkn7qPn4fsmeMcDPifQScf3Jv/ia9j+26X8r/A8v+yZ91/XyNrwR+y1quneK9PvfEmpaFq2kRyE3doomUyoVI4IxyCQevasq2cxlBqCafyNKWVuM05NNH0F4d+HXgXw8yyaP4S0a0lXkSraqZB/wIgn9a8epiq1T4pNnqQw9KG0UdVXObBQAUAFABQAUAFAFTWZbqDTJ5LGEzXW3EKdi54GfYE5PsDQByf2vxbZxixht5GNtG4SadWmNxySuWUfeClepXLA80AS3Wp6/bXMVvLJKFe4aGWQWJYoATsKY++WUbjjO30HSmAsmr+JzZQr/AGfJFc8/aWFuxWMFl2leG3HBOQAcc56UgFlufFCPBcSu43GVJI4bQsigNGA/I3E7fMYZxnpjsQCPUNX8TW+kanLBYXN5dLZObFI7Ypuk3bIy4I6tlXIB4AbIGOQDlNDu/ifp0aaLJb3DtafaZnvb4C4FwgjR4kMwIAJfzEJ7DHpT0AzdH8dfETVVddNghvUgeJZp0scmORod+xlRn+TcSCw5AA6ZzRYDS16/+KtxpV75McsBkjuHiW1sP3sPlXSKihi3zGSIsw4BIXjrRoBIdS+JNtNftp1hKYI5p7lBNZO7XI+0RqqDc+U3Rs7YHTbwBQBZh1zxhd6T4j/tbw/rjQxrE9hDaIbe6dzI4MQdT0G1CWX+EnBaiwHMvF8T1ttlnc+Iri/W0AimkQxwtF9lcPlW487z8BS3zHCnoTT0AuWqeP8A+1bcwjxMLH7Yv9l+ezZEf2pPM+1ZOceTv27+3TnFID2ykAUAFABQAUAIVBxkA4OR7UALQAUAFACEAgggEGgCO2tre1i8q2gihjznbGgUZ+goAloAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgD//Z',
                                                    alignment: 'left'
                                                },
                                                {
                                                    // style: "title",
                                                    text: '\nListado de Planos',
                                                    fontSize: 20,
                                                    color: "#01397f",
                                                    alignment: 'center'
                                                }]
                                            ]
                                        }
                                    });

                                    //Inserto registros de detalle
                                    doc.content[2].width = 'auto';
                                    doc.content[2] = {
                                        columns: [
                                            { width: '*', text: '' },
                                            doc.content[2],
                                            { width: '*', text: '' }
                                        ]
                                    };

                                    // Inserto el footer
                                    doc.footer = {
                                        columns: [{
                                            margin: [5, 5, 5, 5],
                                            alignment: 'right',
                                            text: Date.now().toLocaleString()
                                        }]
                                    };

                                    return doc;
                                }
                            },
                            'excel', 'print'
                        ]
                },
                language: currentLang !== undefined ? currentLang : spanish
            });
    };

    $scope.PlanosDelete = function (ID, Description) {

        var record = ID + (Description || undefined ? " - " + Description : "");

        bAlert($translate.instant("index.planesDeleteTitle") + record,
            $translate.instant("index.usersDeleteQuestion"), "",
            bAlertsLevels.DANGER,
            function () {

                $http.delete(urlBase + "blueprints/" + ID).then(function (a, b, c) {

                    bAlert($translate.instant("index.usersDeletedTitle"),
                        $translate.instant("index.usersDeletedMsgOK"), "", bAlertsLevels.SUCCESS, null, "Cerrar");
                    $('#PlanosTable').DataTable().ajax.reload();
                },function (a, b, c) {

                        bAlert($translate.instant("index.errorTitle"),
                            $translate.instant("index.errroMsg"),
                            $translate.instant("index.pleaseRetry"), bAlertsLevels.WARNING, null, "Cerrar");
                    $('#PlanosTable').DataTable().ajax.reload();
                });
            }
        );
    };
});

app.controller('PlanosViewController', function ($scope, $location, $route, $routeParams, $timeout, $compile, $translate, PlanosFactory) {

    $scope.go = function (url, acciones, data) {
        $location.path(url);

        if (acciones != null || acciones != undefined) {

            PlanosFactory.acciones = acciones;
        };
    };

    $scope.Inicializar = function (accion) {

        if (PlanosFactory.acciones === 'view') {

            $scope.plano = {
                ID: PlanosFactory.data.ID,
                Nombre: PlanosFactory.data.Nombre,
                ID_Estado: PlanosFactory.data.ID_Estado
            };

            document.getElementById("PLANO_ESTADO").checked = PlanosFactory.data.ID_Estado === 1;
        }
    };
});

app.controller('PlanosEditController', function ($scope, $location, $route, $routeParams, $timeout, $compile, $http, $translate,PlanosFactory) {

    $scope.accion = PlanosFactory.acciones;

    $scope.go = function (url, acciones,datos) {
        $location.path(url);
    };

    $scope.plano = {
        ID: PlanosFactory.data.ID,
        Nombre: PlanosFactory.data.Nombre,
        ID_Estado: PlanosFactory.data.ID_Estado
    };

    $scope.chgNombre = function () {

        PlanosFactory.data.Nombre = $scope.plano.Nombre;
    };

    $scope.SaveData = function () {

        if ($scope.accion === 'edit') {

            $scope.plano.ID = PlanosFactory.data.ID;
            $scope.plano.Nombre = PlanosFactory.data.Nombre;
            $scope.plano.ID_Estado = PlanosFactory.data.ID_Estado;

            PlanosFactory.acciones = "edit";
        }
        else {

            $scope.plano.ID_Estado = 1;
            $scope.plano.ID = 0;

            PlanosFactory.acciones = "insert";
        }

        $http({
            method: PlanosFactory.acciones === 'edit' ? 'PUT' : 'POST',
            url: urlBase + 'blueprints',
            data: JSON.stringify($scope.plano)
        }).then(function (a, b, c) {

            bAlert(PlanosFactory.acciones === 'edit' ?
                $translate.instant("index.recordUpdateTitle") :
                $translate.instant("index.recordCreateTitle"),
                $translate.instant("index.recordCreateMsgOK"), '', bAlertsLevels.SUCCESS, null, 'Cerrar');

            if (PlanosFactory.acciones == 'insert') {

                PlanosFactory.data = {

                    ID: a.data.Objeto.ID,
                    Nombre: a.data.Objeto.Nombre,
                    ID_Estado: a.data.Objeto.ID_Estado
                }
            }

            PlanosFactory.acciones = "view";

            $scope.go("planosView", "view", + PlanosFactory.data);
        })
            .catch(function (a, b, c) {

                var errorMessage = $translate.instant("index.recordFail") + "\r\n";

                if (a.data.Errores !== undefined && a.data.Errores.error !== undefined) {

                    errorMessage += a.data.Errors.error;
                }

                bAlert(PlanosFactory.acciones === 'edit' ?
                    $translate.instant("index.recordUpdateTitle") :
                    $translate.instant("index.recordCreateTitle"),
                    errorMessage, '', bAlertsLevels.WARNING, null, 'Cerrar');
            });
    };

    $scope.lblPlanosIU = PlanosFactory.lblPlanosIU;

    $scope.checkEstado = function () {

        PlanosFactory.data.ID_Estado = document.getElementById("PLANO_ESTADO").checked ? 1 : 2;
    };

    $scope.Inicializar = function (accion) {

        if (PlanosFactory.acciones === 'edit') {

            $scope.plano = {
                ID: PlanosFactory.data.ID,
                Nombre: PlanosFactory.data.Nombre,
                ID_Estado: PlanosFactory.data.ID_Estado

            };

            document.getElementById("PLANO_ESTADO").checked = PlanosFactory.data.ID_Estado === 1;
            document.getElementById("divPlano").style.visibility = "visible";
            document.getElementById("divEstado").style.visibility = "visible";
        }
        else {

            document.getElementById("divPlano").style.visibility = "hidden";
            document.getElementById("divEstado").style.visibility = "hidden";
        }
    };
});