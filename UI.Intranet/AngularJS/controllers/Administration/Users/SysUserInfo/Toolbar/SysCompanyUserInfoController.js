﻿app.controller("SysCompanyUserInfoController",
    ['$scope', '$location', '$route', '$routeParams', '$timeout', '$compile', '$http', 'CommonDataFactory',
        function ($scope, $location, $route, $routeParams, $timeout, $compile, $http, CommonDataFactory) {

            $scope.userID = 0;
            $scope.userAlias = '';
            $scope.userName = '';
            $scope.userLastName = '';

            $scope.$on('setUserID', function (evt, data) {

                $scope.userID = userID;
                $scope.userAlias = userAlias;
                $scope.userName = userName;
                $scope.userLastName = userLastName;

                if (!$scope.$$phase) {

                    $scope.$apply();
                }
            });

            $scope.Initialize = function () {

                $scope.userID = userID;
            };

            $scope.changeSign = function (buttonId) {

                if ($("#" + buttonId).hasClass('fa fa-plus')) {
                    $("#" + buttonId).removeClass('fa fa-plus');
                    $("#" + buttonId).addClass('fa fa-minus');
                }
                else {
                    $("#" + buttonId).removeClass('fa fa-minus');
                    $("#" + buttonId).addClass('fa fa-plus');
                };

                if (!$scope.$$phase) {

                    $scope.$apply();
                }
            };

        }]);