﻿app.controller("SysUserLogController",
    ['$scope', '$location', '$route', '$routeParams', '$timeout', '$compile', '$http', 'CommonDataFactory',
        function ($scope, $location, $route, $routeParams, $timeout, $compile, $http, CommonDataFactory) {

            var myLIneChart = null;
            var opcion = '';
            var data = "";
            var elemento;
            var chartOptions = {
                legend: { display: false },
                title: { display: true },
                scales: {
                    xAxes: [{
                        stacked: true,
                        beginAtZero: true
                    }],
                    yAxes: [{
                        stacked: true,
                        beginAtZero: true
                    }]
                }
            };

            $scope.userID = 0;
            $scope.userAlias = '';
            $scope.userName = '';
            $scope.userLastName = '';

            $scope.$on('setUserID', function (evt, data) {

                $scope.userID = userID;
                $scope.userAlias = userAlias;
                $scope.userName = userName;
                $scope.userLastName = userLastName;

                if (!$scope.$$phase) {

                    $scope.$apply();
                }

            });

            $scope.Initialize = function () {

                $scope.userID = userID;
            };

            Chart.defaults.global.defaultFontFamily = "Lato";
            Chart.defaults.global.defaultFontSize = 18;

            $scope.changeSign = function (buttonId) {

                if ($("#" + buttonId).hasClass('fa fa-plus')) {
                    $("#" + buttonId).removeClass('fa fa-plus');
                    $("#" + buttonId).addClass('fa fa-minus');

                    $timeout(function () {

                        if (buttonId === "userLogStatistics") {

                            $scope.changeStatistic("userLogStatisticsToday");

                            if (!$scope.$$phase) {

                                $scope.$apply();
                            }
                        }
                    }, 1000);
                }
                else {
                    $("#" + buttonId).removeClass('fa fa-minus');
                    $("#" + buttonId).addClass('fa fa-plus');

                    if (buttonId === "userLogStatistics") {

                        myLIneChart.destroy();
                    }

                };

                if (!$scope.$$phase) {

                    $scope.$apply();
                }
            };

            $scope.changeStatistic = function (opcion) {

                if (myLIneChart != null || myLIneChart != undefined) {

                    myLIneChart.destroy();
                }

                var radioButton = "";

                var txtTitle = 'Inicios de sesión - ';

                Date.prototype.addDays = function (days) {
                    var date = new Date(this.valueOf());
                    date.setDate(date.getDate() + days);
                    return date;
                }

                var date = new Date();

                switch (opcion) {

                    case "userLogStatisticsToday": {

                        chartOptions.title.text = txtTitle + $.datepicker.formatDate('dd/mm/yy', new Date());

                        data = {

                            labels: ["00:00", "01:00", "02:00", "03:00", "04:00", "05:00", "06:00", "07:00", "08:00", "09:00", "10:00", "11:00",
                                "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00", "21:00", "22:00", "23:59"],

                            datasets: [{
                                label: "Conexiones (minutos)",
                                data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 30, 60, 60, 60, 0, 60, 60, 60, 60, 0, 0, 0, 0, 0, 0],
                            }]
                        };

                        radioButton = document.getElementsByName("userLogStatisticsToday");

                        break;
                    }
                    case "userLogStatisticsLastWeek": {

                        var today = date.addDays(-7);

                        var lastWeek = today.getDate() + "-" + (today.getMonth() + 1) + "-" + today.getFullYear();

                        chartOptions.title.text = txtTitle + ' Semana del ' + lastWeek;

                        data = {
                            labels: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
                            datasets: [{
                                label: "Car Speed (mph)",
                                data: [0, 59, 75, 20, 20, 55, 40],
                            }]
                        };

                        radioButton = document.getElementsByName("userLogStatisticsLastWeek");

                        radioButton.checked = true;

                        break;
                    }
                    case "userLogStatisticsLastMonth": {

                        const monthNames = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
                            "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"
                        ];

                        var today = date.addDays(-30);

                        chartOptions.title.text = txtTitle + monthNames[today.getMonth()] + ' de ' + today.getFullYear();

                        data = {
                            labels: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"],
                            datasets: [
                                {
                                    data: [10, 40, 30, 20, 5, 3, 8, 2, 60, 21, 17, 26, 54, 81, 12, 3, 26, 45, 37, 18, 1, 66, 25, 52, 84, 115, 9, 66, 33, 7],
                                    backgroundColor: "#D6E9C6" // green
                                },
                                {
                                    data: [25, 36, 4, 9, 2, 76, 18, 3, 99, 55, 07, 01, 45, 18, 21, 3, 62, 54, 73, 81, 11, 99, 52, 25, 48, 511, 6, 99, 66, 14],
                                    backgroundColor: "#EBCCD1" // green
                                },
                                {
                                    data: [10, 40, 30, 20, 5, 3, 8, 2, 60, 21, 17, 26, 54, 81, 12, 3, 26, 45, 37, 18, 1, 66, 25, 52, 84, 115, 9, 66, 33, 7],
                                    backgroundColor: "#AABBCC" // green
                                },
                                {
                                    data: [10, 40, 30, 20, 5, 3, 8, 2, 60, 21, 17, 26, 54, 81, 12, 3, 26, 45, 37, 18, 1, 66, 25, 52, 84, 115, 9, 66, 33, 7],
                                    backgroundColor: "#DDFFEE" // green
                                }
                            ]
                        };

                        radioButton = document.getElementsByName("userLogStatisticsLastMonth");

                        radioButton.checked = true;

                        break;
                    }
                    case "userLogStatisticsLastYear": {

                        var today = date;

                        chartOptions.title.text = txtTitle + today.getFullYear();

                        data = {
                            labels: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic",],
                            datasets: [{
                                label: "Car Speed (mph)",
                                data: [59, 75, 20, 20, 55, 40, 36, 65, 22, 11, 9, 17],
                            }]
                        };

                        radioButton = document.getElementsByName("userLogStatisticsLastYear");

                        radioButton.checked = true;

                        break;
                    }
                }

                elemento = $("#myChartUserLog")[0];

                myLIneChart = new Chart(elemento, {
                    type: 'bar',
                    data: data,
                    options: chartOptions
                });

                if (!$scope.$$phase) {

                    $scope.$apply();
                }
            }

        }]);