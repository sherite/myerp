﻿app.controller("AccountsBankSetupController", function ($scope, $location) {

    $scope.go = function (url, actions, data) {

        $location.path(url);
    };

    $scope.Initialize = function () {

    };

    $scope.clickToOpen = function (template) {

        $('#' + template).modal('show');
    };

});