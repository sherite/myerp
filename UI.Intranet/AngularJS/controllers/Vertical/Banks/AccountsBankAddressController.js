﻿app.controller("AccountsBankAddressController", function ($scope, $location, $route, $routeParams, $timeout, $compile, $http, CommonDataFactory) {

    $scope.go = function (url, actions, data) {

        $location.path(url);
    };

    $scope.Initialize = function () {

    };

    $scope.clickToOpen = function (template) {

        $('#' + template).modal('show');
    };

});