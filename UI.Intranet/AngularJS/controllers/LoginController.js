﻿app.factory('LoginService', function () {
    var admin = 'admin';
    var pass = 'pass';
    var isAuthenticated = false;

    return {
        login: function (username, password) {
            isAuthenticated = username === admin && password === pass;
            return isAuthenticated;
        },
        isAuthenticated: function () {
            return isAuthenticated;
        }
    };

});

app.controller('LoginController', function ($scope, $rootScope, $location, $translate, LoginService) {

    $scope.$on('event:social-sign-in-success', (event, userDetails) => {
        $scope.result = userDetails;

        if (!$scope.$$phase) {

            $scope.$apply();
        }


        $scope.username = "admin";
        $scope.password = "pass";
        $scope.formSubmit();

    })

    $scope.$on('event:social-sign-out-success', function (event, userDetails) {
        $scope.result = userDetails;
    })

    $scope.Initialize = function () {

        $('#cover-spin').show(0);

        $('[data-toggle="push-menu"]').pushMenu('toggle');
        document.getElementById("pushButton").disabled = true;
        document.getElementById("pushButton").style.visibility = "hidden";
        document.getElementById("userMenu").style.visibility = "hidden";
        document.getElementById("leftBar").style.visibility = "hidden";
        document.getElementById("myBody").className = "skin-blue sidebar-collapse";

        if (IsOpenPushButton) {

            $('[data-toggle="push-menu"]').pushMenu('toggle');
            document.getElementById("pushButton").disabled = true;
            IsOpenPushButton = false;
        }

        $('#cover-spin').hide(0);

    };

    $scope.formSubmit = function () {
        if (LoginService.login($scope.username, $scope.password)) {
            $scope.error = '';
            $scope.username = '';
            $scope.password = '';

            IsLogin = true;

            $location.path('/dashboard');

            document.getElementById("pushButton").disabled = false;
            document.getElementById("pushButton").style.visibility = "visible";
            document.getElementById("userMenu").style.visibility = "visible";
            document.getElementById("myBody").className = "skin-blue sidebar-mini";
            document.getElementById("leftBar").style.visibility = "visible";

        } else {
            $scope.error = "Incorrect username/password !";
        }
    };
});

