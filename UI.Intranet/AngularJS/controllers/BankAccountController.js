﻿app.controller("BankAccountsController", function ($scope, $location, $route, $routeParams, $timeout, CommonDataFactory) {

    $scope.go = function (url, actions, data) {

        $location.path(url);
    };

    $scope.Initialize = function () {

        $scope.go('/accountsBankOverview');
    };
});