﻿app.controller("IndexController", function ($scope, $http, $translate, $location) {

    $scope.dynamicMenu = [];

    $scope.changeLanguage = function (lang) {

        switch (lang) {
            case "es":
                language = "es-es";
                break;
            case "br":
                language = "br-br";
                break;
            case "fr":
                language = "fr-fr";
                break;
            case "de":
                language = "de-de";
                break;
            case "it":
                language = "it-it";
                break;
            case "en":
                language = "en-us";
                break;
        }

        $translate.use(language);

        $scope.$broadcast('languageChanged', language);
    };

    $scope.go = function (url, action, data) {

        $location.path("/" + url);

    };

    $scope.ButtonPushed = function () {

        IsOpenPushButton = !IsOpenPushButton;
    };

    $scope.Initialize = function () {

        $('#cover-spin').show(0);

        $('[data-toggle="push-menu"]').pushMenu('toggle');

        IsOpenPushButton = false;

        $scope.CreateDynamicMenu();

        $scope.go('login');
    };

    $scope.CreateDynamicMenu = function () {

        $http({
            url: urlBase + "dynamicMenu",
            method: 'GET'
        }).then(
            function successCallback(response) {

                var lista = document.getElementById("tvwSetup");

                var result = "";

                for (var i = 0; i < response.data.length; i++) {

                    result += response.data[i];
                }

                lista.insertAdjacentHTML("beforebegin", result);

            });
    };

    $scope.IsLogin = function () {

        return IsLogin;
    };
});

$('ul').on('expanded.tree', function (e) {

    var id = e.target.attributes[1].ownerDocument.activeElement.firstElementChild.id;

    var elemento = $("#" + id);
    
    elemento.removeClass('fa fa-folder').addClass('fa fa-folder-open');
});

$('ul').on('collapsed.tree', function (e) {

    var id = e.target.attributes[1].ownerDocument.activeElement.firstElementChild.id;

    var elemento = $("#" + id);
    
    elemento.removeClass('fa fa-folder-open').addClass("fa fa-folder");
});