﻿var mnuBank = {
    'Places': {
        'BankAccounts': ''
    },
    'Journals': {
        'CheckReversals': '',
        'DepositSplitPaymentCancellations': ''
    },
    'Reports': {
        'Transactions': {
            'BankAccountStatement': ''
        },
        'Statistics': {
            'PaymentSummaryByDate': '',
            'DepositSummaryByDate': '',
            'PaymentSummaryByVendor': '',
            'DepositSummaryByCustomer': '',
            'AccountBalance': ''
        },
        'External': {
            'PaymentAdvice': '',
            'DepositSlip': ''
        },
        'Reconciliation': {
            'UnreconciliedBankTransactions': ''
        }
    },
    'CommonForms': {
        'BankAccountDetails': '',
        'DepositSlip': '',
        'Checks': '',
        'BillOfExchangeTable': '',
        'PromissoryNote': ''
    },
    'Inquiries': {
        'BankTransactions': '',
        'BalanceControl': '',
        'BankRemittance': ''
    },
    'Setup': {
        'BankTransactionType': '',
        'BankTransactionGroups': '',
        'BankGroups': '',
        'PaymentPurposeCodes': '',
        'BankReasons': '',
        'Parameters': ''
    }
}
var mnuCRM = {
    'Places': {
        'BusinessRelations': {
            'MyBusinessRelations': ''
        },
        'Contacts': { 'MyContacts': '' },
        'Leads': {
            'New Leads': '',
            'Active Leads': '',
            'MyLeads': '',
            'MyNewLeads': '',
            'MyActiveLeads': ''
        },
        'Opportunities': {
            'MyNewOpportunities': '',
            'MyActiveOpportunities': '',
            'NewOpportunities': '',
            'ActiveOpportunities': '',
            'MyOpportunities': ''
        },
        'Quotations': {
            'ExpiringQuotations': '',
            'MyQuotations': '',
            'MyExpiringQuotations': ''
        },
        'Customers': '',
        'Campaigns': {
            'MyCampaigns': ''
        },
        'CallLists': { 'MyCallLists': '' },
        'Competitors': '',
        'Activities': {
            'MyActivities': '',
            'ActivitiesPastDue': '',
            'ActivitiesDueThisWeek': '',
            'MyActivitiesPastDue': '',
            'MyActivitiesDueThisWeek': ''
        },
        'GlobalAddressBook': ''
    },
    'Periodic': {
        'Documents': '',
        'Update': {
            'UpdateContacts': '',
            'UpdateFinancial': ''
        },
        'Import': {
            'Contacts': '',
            'Leads': '',
            'BusinessSectors': ''
        },
        'Mailings': {
            'CreateMailingFile': '',
            'GenerateMailings': '',
            'Mailings': '',
            'MailingMergeFiles': '',
            'EmailDistribution': ''
        },
        'SalesManagement': {
            'ManagementStatistics': '',
            'SalesTarget': ''
        },
        'MarketingAutomation': {
            'CampaignDetails': '',
            'Encyclopedia': ''
        },
        'Telemarketing': {
            'CallListDetails': '',
            'Telemarketing': ''
        },
        'Synchronization': {
            'Synchronize': '',
            'MyContacts': '',
            'Migrate': ''
        },
    },
    'Reports': {
        'SalesForceAutomation': {
            'ContactManagement': {},
            'Quotations': {},
            'Telephony': {}
        },
        'SalesManagement': {
            'ActivityAnalysis': '',
            'ActivityTurnover': '',
            'SalesTargets': '',
            'ActivityPerQuotation': '',
            'ListOfActivities': '',
            'BusinessRelationTurnover': ''
        },
        'Campaigns': {
            'ResponseFrequency': '',
            'Targets': '',
            'TargetResponded': ''
        },
        'Telemarketing': {
            'CallListReport': '',
            'StatusReport': '',
            'Response': '',
            'Summary': ''
        }
    },
    'CommonForms': {
        'BusinessRelationDetails': '',
        'ContactDetails': '',
        'LeadDetails': '',
        'OpportunityDetails': '',
        'SalesQuotationDetails': '',
        'ActivityDetails': '',
        'GlobalAddressBookDetails': ''
    },
    'Inquiries': {
        'ConvertedBusinessRelations': '',
        'TransactionLog': '',
        'Calls': ''
    },
    'Setup': {
        'EmployeeDetails': '',
        'Responsibilities': '',
        'WorkflowConfigurations': '',
        'Parameters': '',
        'SourceType': '',
        'Leads': {
            'Type': '',
            'Priority': '',
            'Rating': '',
            'QualifyProcess': ''
        },
        'Opportunity': {
            'Phase': '',
            'Prognosis': '',
            'Probability': '',
            'Reason': '',
            'CompetitorDetails': '',
            'SalesProcess': ''
        },
        'ContactManagement': {
            'BusinessRelations': {
                'RelationTypes': '',
                'Status': '',
                'CustomerGroups': '',
                'VendorGroups': '',
                'SalesDistricts': '',
                'Segments': '',
                'SubSegments': '',
                'BusinessSectors': '',
                'CompanyChains': ''
            },
            'Contacts': {
                'JobTitle': '',
                'FunctionsOfPersons': '',
                'Character': '',
                'Decision': '',
                'Interest': '',
                'Loyalty': '',
                'ComplimentaryClose': '',
                'Salutation': ''
            },
            'Activities': {
                'phases': '',
                'plans': '',
                'types': ''
            },
            'Quotations': {
                'DocumentTitles': '',
                'DocumentIntroductions': '',
                'DocumentConclusions': ''
            },
            'Mailings': {
                'Category': '',
                'Items': '',
            },
            'EmailGroups': {
                'Category': '',
                'Groups': '',
            },
            'Telephony': {
                'PhoneParameters': ''
            }
        },
        'SalesManagement': {
            'QueryAdministration': '',
            'SalesUnitOrTeam': ''
        },
        'Campaigns': {
            'Type': '',
            'Group': '',
            'Target': '',
            'MediaType': '',
            'ReasonCanceled': '',
            'EmailTemplate': '',
            'CampaignProcess': ''
        },
        'Telemarketing': {
            'ReasonCanceled': ''
        },
        'Sinchronization': {
            'Parameters': '',
            'Wizard': '',
            'Mappings': '',
            'Administration': ''
        },
    }
}
var mnuAdministration = {
    'Places': {
        'LocationView': '',
        'OrganizationView': '',
        'ReportingRelationshipsView': ''
    },
    'Periodic': {
        'EmailBroadcast': '',
        'NotificationCleanup': '',
        'CompileApplication': '',
        'DatabaseAdministration': '',
        'DataExportImport': {
            'DefiningGroups': '',
            'ExportTo': '',
            'Import': '',
            'ExportImportCleanup': '',
            'DefaultData': {
                'ExportTo': '',
                'ImportWizard': ''
            },
            'SpreadSheets': {
                'TemplateWizard': '',
                'Export': '',
                'Import': ''
            }
        },
        'EmailProcessing': {
            'EmailSendingStatus': '',
            'RetrySchedule': '',
            'Batch': ''
        },
        'CompanyCurrencyConversion': {
            'Check': {
                'CheckLedgerTransactions': '',
                'Reconciliation': {
                    'SalesTax': '',
                    'Bank': '',
                    'Customer': '',
                    'Vendor': '',
                    'Inventory': ''
                },
                'Settlements': {
                    'Customer': '',
                    'Vendor': '',
                    'Inventory': '',
                }
            },
            'RecalculationsAfterConversion': {
                'Journalizing': '',
                'BusinessStatistics': ''
            }
        },
        'BusinessAnalysis': {
            'UpdateReportModel': '',
            'CreateAllSecureViews': ''
        },
        'System': {
            'DimensionInconsistencyCleanup': ''
        }
    },
    'Reports': {
        'DatabaseLogSetup': '',
        'WorkflowInstanceByStatus': '',
        'AlertTracking': '',
        'WorkflowTracking': '',
        'Tables': '',
        'DatabaseInformation': '',
        'AlertSetup': '',
        'DatabaseLog': '',
        'SizeOfCompanyAccounts': '',
        'Security': {
            'ObjectPermissions': '',
            'UserPermissions': '',
            'UserGroupPermissions': '',
            'RecordLevelSecurity': '',
            'LastLogon': '',
            'OnlineTime': '',
        },
        'System': {
            'MultisiteActivationReadiness': '',
            'ElementUsageLog': ''
        }
    },
    'CommonForms': {
        'Users': '',
        'OnlineUsers': '',
        'CompanyAccounts': ''
    },
    'Inquiries': {
        'UserLog': '',
        'DatabaseLog': '',
        'DigitalSignatureLog': '',
        'UserPermissions': '',
        'Database': {
            'TraceLog': '',
            'DatabaseInformation': ''
        }
    },
    'Setup': {
        'UserGroups': '',
        'UserRelations': '',
        'UserProfiles': '',
        'Domains': '',
        'VirtualCompanyAccounts': '',
        'DatabaseLog': '',
        'DigitalSignatureAuditTrail': '',
        'ServerConfiguration': '',
        'BatchGroups': '',
        'ClusterConfiguration': '',
        'EmailParameters': '',
        'HelpUpdateParameters': '',
        'DigitalSignature': '',
        'DirectoryView': '',
        'WorkflowInfrastructureConfigurationWizard': '',
        'ShippingCarrierRoleUsers': '',
        'BusinessAnalysis': {
            'OLAP': {
                'OLAPAdministration': '',
                'SetupExchangeRates': '',
                'BIProjectGenerationOptions': '',
                'GenerateBIProject': '',
                'UpdateBIProject': '',
                'TimePeriods': '',
            },
            'ReportingServices': {
                'ReportingServices': '',
                'DefaultReportServers': '',
                'ReportModelGenerationOptions': ''
            }
        },
        'Security': {
            'UserGroupPermissions': '',
            'RecordLevelSecurity': '',
            'SystemServiceAccounts': ''
        },
        'System': {
            'CustomerFeedbackOptions': '',
            'LicenseInformation': '',
            'Configuration': '',
            'ModifyDataTypes': '',
            'ConcurrencyModelConfiguration': '',
            'MultisiteActivation': '',
            'StandardCostConversions': '',
            'Checklists': {
                'InitializationChecklist': '',
                'UpgradeChecklist': '',
                'VCSSetupChecklist': '',
                'PreventStartupOfLists': ''
            }
        },
        'Internet': {
            'ExternalWebUsers': '',
            'EnterprisePortal': {
                'ConfigurationWizard': '',
                'ManageDeployments': '',
                'PublishImages': '',
                'WebSites': '',
                'TransactionSummary': '',
                'Parameters': '',
                'CustomerSelfService': {
                    'SalesBasket': '',
                    'SignUp': '',
                    'Parameters': ''
                },
                'ProductCatalogManager': {
                    'ProductGroups': '',
                    'Presentations': ''
                }
            }
        }
    }
}
var mnuBase = {
    'Places': {
        'Employees': {
            'ContractWorkers': '',
            'PastWorkers': ''
        },
        'Positions': {
            'ActivePositions': ''
        },
        'EmployeeJobs': {
            'ActiveEmployeeJobs': ''
        },
        'Organizations': '',
        'GlobalAddressBook': '',
        'OrganizationView': '',
        'ReportingRelatinshipsView': ''
    },
    'Inquiries': {
        'BatchJob': '',
        'BatchJobHistory': '',
        'PrintArchive': '',
        'GanttTable': '',
        'WorkflowHistory': '',
        'WorkCenters': {
            'CapacityReservations': '',
            'CapacityLoadGraphically': ''
        },
        'SynchronizationService': {
            'QueueManager': ''
        },
        'ReportManager': '',
        'ReportBuilder': ''
    },
    'Periodic': {
        'CalendarCleanup': '',
        'ConsistencyCheck': '',
        'GlobalAddressBook': {
            'UpdateContacts': ''
        },
        'ApplicationIntegration': {
            'QueueManager': '',
            'DocumentHistory': '',
            'Exceptions': ''
        },
        'Batch': {
            'Processing': ''
        },
        'ForeignTrade': {
            'Intrastat': '',
            'EUSalesList': ''
        },
        'Alerts': {
            'ChangeBasedAlerts': '',
            'DueDateAlerts': ''
        },
        'Synchronization': {
            'Synchronize': '',
            'MyContacts': '',
            'Migrate': ''
        }
    },
    'CommonForms': {
        'EmployeeDetails': '',
        'OrganizationDetails': '',
        'WorkCenterGroups': '',
        'BatchJobListUser': '',
        'Calendar': '',
        'Dimensions': '',
        'ProcessTemplates': '',
        'ActivityDetails': '',
        'GlbalAddressBookDetails': ''
    },
    'Reports': {
        'BaseData': {
            'Organization': '',
            'Position': '',
            'Job': ''
        },
        'WorkCenters': {
            'WorkCenters': '',
            'CapacityReservations': ''
        },
        'Calendar': {
            'Calendar': '',
            'WorkingTimeTemplates': ''
        }
    },
    'Setup': {
        'CompanyInformation': '',
        'JobTitle': '',
        'Job': '',
        'Position': '',
        'LimitTypes': '',
        'TransactionText': '',
        'RecordTemplates': '',
        'BarCodeSetup': '',
        'WorkTasks': '',
        'EmailTemplates': '',
        'Reasons': '',
        'SettingsForWorkflow': '',
        'RoleCenter': {
            'EditCues': '',
            'EditQuickLinks': '',
            'InitializeRoleCenterProfiles': ''
        },
        'ApplicationIntegration': {
            'GlobalSettings': '',
            'LocalEndpoints': '',
            'TransportAdapters': '',
            'Channels': '',
            'Services': '',
            'ServiceReferences': '',
            'Endpoints': '',
            'Actions': '',
            'ValueLookup': '',
            'WebSites': '',
            'XSLTRepository': ''
        },
        'Import': {
            'GlobalSettings': '',
            'Documents': '',
            'Transformantions': '',
            'Formats': ''
        },
        'NumberSequences': {
            'NumberSequences': '',
            'References': '',
            'NumberSequenceGroups': ''
        },
        'Addresses': {
            'PostalCodes': '',
            'CountryRegion': '',
            'AddressFormat': '',
            'ContactDetails': '',
            'JobTitle': '',
            'PersonalTitle': ''
        },
        'Activities': {
            'Phases': '',
            'Plans': '',
            'Types': ''
        },
        'DocumentManagement': {
            'DocumentTypes': '',
            'ActiveDocumentTables': '',
            'Parameters': ''
        },
        'GlobalAddressBook': {
            'Suffix': '',
            'Salutation': '',
            'CommunicationMethods': '',
            'Relationships': '',
            'Parameters': ''
        },
        'Categories': {
            'Categories': ''
        },
        'Units': {
            'Units': '',
            'UnitConversion': '',
            'FixedUnits': ''
        },
        'Calendar': {
            'Periods': '',
            'WorkingTimeTemplates': ''
        },
        'ForeignTrade': {
            'CommodityCodes': '',
            'TransactionCodes': '',
            'TransportMethod': '',
            'Port': '',
            'StatisticsProcedure': '',
            'IntrastatParameters': '',
        },
        'Compensation': {
            'Levels': '',
            'ReferencePointSetups': '',
            'CompensationGrids': ''
        },
        'BusinessAnalysis': {
            'ReportBuilderOptions': ''
        },
        'DataCrawler': {
            'TableSetup': '',
            'DataCrawler': ''
        },
        'Alerts': {
            'ManageAlertRules': '',
            'AlertParameters': ''
        },
        'Synchronization': {
            'SetupWizard': '',
            'Mapping': '',
            'Administration': ''
        },
        'SynchronizationService': {
            'Parameters': ''
        }
    }
}
var mnuHumanResources = {
    'Places': {
        'OrganizationView': '',
        'ReportingRelationshipsView': '',
        'RecruitmentProjects': {
            'ExpiringRecruitmentProjects': '',
            'ExpiringNearingApplicationDeadline': ''
        },
        'EmployeeActions': {
            'EmployeeActionsThatAreDue': ''
        },
        'Courses': {
            'ExpiringCourses': '',
            'ExpiringNearingRegistrationDeadLine': ''
        },
        'CourseParticipants': '',
        'EmployeeCertificates': {
            'DueForRenewal': ''
        },
        'Employees': {
            'WithExpiringProbationPeriod': '',
            'ContractWorkers': '',
            'PastWorkers': ''
        },
        'LoanedEquipments': {
            'AboutToExpire': ''
        },
        'EmployeeJobs': '',
        'Organizations': '',
        'Positions': '',
        'ApplicationsBasket': '',
        'Applications': {
            'NewApplications': '',
            'ApplicationsDueForDeletion': ''
        },
        'ApplicantInterviews': {
            'ThatAreDue': ''
        },
        'AppraisalInterviews': {
            'ThatAreDue': ''
        },
        'StrategicPlans': {
            'ThatAreDue': ''
        },
        'GlobalAddressBook': '',
        'EmployeeGoals': {
            'ThatAreDue': ''
        }
    },
    'Journals': {
        'Absence': {
            'Request': '',
            'RequestApproval': '',
            'Registration': '',
            'MultiRegistration': '',
            'Approval': ''
        }
    },
    'Reports': {
        'NumberOfEmployees': '',
        'Goals': '',
        'SkillsGapJobPerson': '',
        'SkillsGapPersonJob': '',
        'SkillsGapEmployeeGoal': '',
        'SkillGapJob': '',
        'SkillProfile': '',
        'EmployeeBenefits': '',
        'StrategicPlans': '',
        'AbsenceAdministration': '',
        'Accommodation': '',
        'BaseData': {
            'RecruitmentProjects': '',
            'ActionPlans': '',
            'SeniorityList': '',
            'Resume': {
                'EmployeeResume': '',
                'ApplicantResume': '',
                'NetworkResume': ''
            },
            'Organization': {
                'JobsPerOrganizationUnit': '',
                'JobInformation': '',
                'PeoplePerOrganizationUnit': '',
                'PeoplePerJob': '',
                'GroupsPerLineOrganizationUnit': ''
            }
        },
        'Transactions': {
            'Actions': '',
            'StrategicPlanningDevelopments': ''
        },
        'StatusLists': {
            'EmployeeResponsibilities': '',
            'HiredInPeriod': '',
            'TerminatedInPeriod': '',
            'ApplicationStatusByProject': '',
            'AbsenceStatus': '',
            'EmployeeLeave': '',
            'ApplicantStatus': '',
            'Skills': {
                'SkillsByPerson': '',
                'JobsPerSkill': '',
                'CourseTypesPerSkill': '',
                'EmployeeSkillsBySkillType': '',
                'SkillCountBySkillType': ''
            },
            'Course': {
                'CourseConfirmation': '',
                'ListOfParticipants': '',
                'WaitingList': '',
                'CourseCertificate': '',
                'QuestionnaireResult': '',
                'Registration': ''
            }
        },
        'AlarmLists': {
            'ExpiredCertificates': '',
            'OverdueLoan': '',
            'AbsenceJournalMissing': '',
            'Goal': '',
            'Actions': '',
        },
        'Celebrations': {
            'Birthdays': '',
            'Anniversaries': ''
        },
        'SetupLists': {
            'SkillTypes': '',
            'EducationGroup': '',
            'AbsenceSetup': '',
            'ReferenceTypes': '',
            'MassHireProject': '',
            'Course': {
                'Structure': '',
                'Instructors': '',
                'Agenda': '',
                'Registration': ''
            },
            'Network': {
                'Groups': ''
            }
        }
    },
    'Setup': {
        'ReferenceTypes': '',
        'NetworkGroups': '',
        'ReasonCodes': '',
        'Parameters': '',
        'Compensation': {
            'PerformancePlans': '',
            'PerformanceRatings': '',
            'Levels': '',
            'ReferencePointSetups': '',
            'FixedCompensationActions': '',
            'Locations': '',
            'JobFunctions': '',
            'JobTypes': '',
            'PayFrequences': '',
            'ExternalPayGroups': '',
            'VariableCompensationTypes': '',
            'VestingRules': '',
            'SurveyCompanies': '',
            'CompensationGrids': ''
        },
        'Organization': {
            'WorkTasks': '',
            'AreasOfResponsability': '',
            'JobTemplates': ''
        },
        'Employee': {
            'Certificates': '',
            'LanguageCodes': '',
            'Unions': '',
            'RecordTypes': '',
            'AccommodationTypes': '',
            'EthnicOrigin': '',
            'VeteranStatus': '',
            'IdentificationTypes': '',
            'Identification': '',
            'Skills': {
                'SkillTypes': '',
                'Skills': '',
                'RatingModels': ''
            },
            'Education': {
                'Education': '',
                'Institutions': '',
                'Degrees': '',
                'EducationGroup': ''
            },
            'Loan': {
                'LoanItems': '',
                'LoanTypes': ''
            },
            'Development': {
                'GoalHeadings': '',
                'GoalTypes': '',
                'InterviewTypes': ''
            },
            'Payroll': {
                'PayrollCategory': '',
                'IncomeTaxCodes': '',
                'BenefitTypes': '',
                'SalaryDeductionTypes': '',
                'InsuranceTypes': '',
                'IncomeTaxCategory': '',
                'TermsOfEmployment': '',
                'PersonnnelCategory': '',
                'LeaveTypes': '',
                'PayrollAllowance': ''
            }
        },
        'Recruitment': {
            'MediaTypes': '',
            'Media': '',
            'ApplicationBookmarks': '',
            'ApplicationEmailTemplates': ''
        },
        'Absence': {
            'Status': '',
            'Codes': '',
            'Groups': '',
            'Setup': ''
        },
        'Course': {
            'Groups': '',
            'Types': '',
            'Locations': '',
            'ClassroomGroups': '',
            'Classrooms': '',
            'Instructors': ''
        },
        'Strategy': {
            'StrategicPlanTypes': '',
            'StrategicPlanningTemplates': ''
        },
        'Actions': {
            'ActionGroups': '',
            'ActionTypes': '',
            'ResponseTypes': '',
            'ActionTypeTemplates': ''
        },
    },
    'CommonForms': {
        'OrganizationDetails': '',
        'JobDetails': '',
        'PositionDetails': '',
        'EmployeeDetails': '',
        'RecruitmentProjectDetails': '',
        'Actions': '',
        'GlobalAddressBookDetails': ''
    },
    'Inquiries': {
        'Network': '',
        'AbsenceAdministration': '',
        'Transactions': {
            'ApplicantInterview': '',
            'CourseParticipants': '',
            'Absence': {
                'History': '',
                'Transactions': ''
            }
        },
        'Statistics': {
            'EmployeeDistribution': '',
            'AbsenceStatistics': '',
            'CourseStatistics': '',
            'ApplicationStatistics': '',
            'ActionStatistics': '',
            'Skill': {
                'Network': '',
                'Organization': '',
                'SkillGapAnalysisJobPerson': '',
                'SkillProfileAnalysis': ''
            }
        }
    },
    'Periodic': {
        'CourseDetails': '',
        'Loan': '',
        'SkillMappingProfiles': '',
        'SkillMapping': '',
        'MassHireProject': '',
        'ApplicationBasketDetails': '',
        'Compensation': {
            'FixedCompensationPlans': '',
            'VariableCompensationPlans': '',
            'ElegibilityRules': '',
            'PayForPerformanceAllocations': '',
            'ProcessEvent': '',
            'Events': '',
            'FixedIncreaseBudgets': ''
        },
        'Administration': {
            'Applicants': '',
            'ApplicationDetails': '',
            'EmployeeRecords': '',
            'CreateApplicationDocument': '',
            'EmailApplicant': ''
        },
        'Planning': {
            'AppraisalInterviewDetails': '',
            'StrategicPlanDetails': '',
            'ActionPlans': '',
            'Goals': ''
        },
        'MassCreation': {
            'CreationJobs': '',
            'CreateLoanItems': '',
            'CreateAbsenceJournals': '',
            'CreateActions': ''
        },
        'Update': {
            'UpdateNetwork': '',
            'UpdateEmployeeStatus': '',
            'UpdateApplicationExpireDate': ''
        },
        'Cleanup': {
            'Delete absence journals': '',
            'Delete actions': '',
            'Delete personal information on employee': '',
            'Delete personal information on applicant': ''
        }
    }
}