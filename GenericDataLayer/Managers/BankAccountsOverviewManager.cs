﻿namespace GenericDataLayer.Managers
{
    using System;
    using GenericDataLayer.Helpers;
    using Configuration;
    using Microsoft.Win32.SafeHandles;
    using System.Runtime.InteropServices;
    using System.Reflection;
    using Entities.Helpers;
    using Entities;
    using System.Collections.Generic;
    using PagedList;

    public class BankAccountsOverviewManager : IDisposable, IBankAccountsOverviewManager
    {
        public MSSQLDataLayer.BankAccountOverview BankAccountsOverview { get; set; }

        public BankAccountsOverviewManager(MSSQLDataLayer.BankAccountOverview bankAccountsOverview)
        {
            this.BankAccountsOverview = bankAccountsOverview ?? new MSSQLDataLayer.BankAccountOverview();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public IList<Entities.DTOs.BankAccountOverview> Select()
        {
            return BankAccountsOverview.Select();
        }
    }
}