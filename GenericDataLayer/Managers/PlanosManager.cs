﻿namespace GenericDataLayer.Managers
{
    using System;
    using System.Collections.Generic;
    using Entities;
    using Entities.Helpers;
    using GenericDataLayer.Helpers;
    using static Entities.Enums;

    /// <summary>
    /// Manager de Planos
    /// </summary>
    public class PlanosManager : IDisposable
    {
        bool disposed = false;

        /// <summary>
        /// Constructor
        /// </summary>
        public PlanosManager()
        {

        }

        /// <summary>
        /// Busca un plano determinado
        /// </summary>
        /// <param name="id">id de plano a buscar</param>
        /// <returns>lista con el plano correspondiente al id pasado como parametro o vacia</returns>
        public List<BluePrint> Find(ulong id)
        {
            object[] parametros = new object[] { id };

            var lstPlano = ReturnListaGenerica("Planos", "Find", parametros);

            return lstPlano;
        }

        /// <summary>
        /// Selecciona una lista de planos
        /// </summary>
        /// <param name="paginado">objeto paginado</param>
        /// <param name="orden">objeto ordenamiento</param>
        /// <param name="idPlano">id del plano</param>
        /// <returns></returns>
        public List<BluePrint> Select(Paging paginado, Ordering orden, ulong? idPlano)
        {
            var plano = new BluePrint();

            var lstPlano = new SPS_SDL_SQL.Planos().Select(paginado, orden, idPlano);

            var retVal = new List<BluePrint>();

            foreach (var item in lstPlano)
            {
                plano = new BluePrint()
                {
                    ID = item.ID,
                    ID_Estado = item.ID_Estado,
                    Name = item.Name
                };

                retVal.Add(plano);
            }

            return retVal;
        }

        /// <summary>
        /// Crea un nuevo plano
        /// </summary>
        /// <param name="plano">plano a insertar</param>
        /// <returns>plano insertado</returns>
        public MgrResponse<BluePrint> Insert(BluePrint plano)
        {
            var mgrResponse = new MgrResponse<BluePrint>();

            var json = Newtonsoft.Json.JsonConvert.SerializeObject(plano);
            var json2Bytes = System.Text.Encoding.UTF8.GetBytes(json);
            var param = System.Convert.ToBase64String(json2Bytes);

            var result = new SPS_SDL_SQL.Planos().Insert(param);

            var miPlano = new BluePrint()
            {
                ID = result.ID,
                ID_Estado = (BluePrintStaus)result.ID_Estado,
                Name = result.Name
            };

            mgrResponse.Object = miPlano;

            return mgrResponse;
        }

        /// <summary>
        /// Actualiza un plano
        /// </summary>
        /// <param name="plano">plano a actualizar</param>
        /// <returns>plano actualizado</returns>
        public MgrResponse<BluePrint> Update(BluePrint plano)
        {
            var mgrResponse = new MgrResponse<BluePrint>();

            var json = Newtonsoft.Json.JsonConvert.SerializeObject(plano);
            var json2Bytes = System.Text.Encoding.UTF8.GetBytes(json);
            var param = System.Convert.ToBase64String(json2Bytes);

            var result = new SPS_SDL_SQL.Planos().Update(param);

            var miPlano = new BluePrint()
            {
                ID = result.ID,
                ID_Estado = (BluePrintStaus)result.ID_Estado,
                Name = result.Name
            };

            mgrResponse.Object = miPlano;

            return mgrResponse;

        }

        /// <summary>
        /// Elimina lógicamente un plano
        /// </summary>
        /// <param name="idPlano">id del plano a eliminar</param>
        /// <returns>estado de la eliminacion</returns>
        public BluePrintStaus Delete(ulong idPlano)
        {
            var retVal = new SPS_SDL_SQL.Planos().Delete((int)idPlano);

            return BluePrintStaus.Inactive;
        }

        /// <summary>
        /// Dispose this object
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// implementation of dispose
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
               // Free any other managed objects here.
                //
            }

            disposed = true;
        }

        /// <summary>
        /// carga dll
        /// </summary>
        public List<BluePrint> ReturnListaGenerica(string entidad, string metodo, object[] parametros)
        {
            var retVal = new List<BluePrint>();


            return retVal;
        }
    }
}