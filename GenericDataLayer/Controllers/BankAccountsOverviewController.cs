﻿namespace GenericDataLayer.Controllers
{
    using GenericDataLayer.Configuration;
    using GenericDataLayer.Helpers;
    using GenericDataLayer.Managers;
    using NLog;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Http;
    using System.Web.Http.Cors;

    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class BankAccountsOverviewController : ApiController
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private static readonly int idEntity = Convert.ToInt32(ConfigurationHandler.idEntity());
        private readonly IBankAccountsOverviewManager bankAccountsOverviewManager = new BankAccountsOverviewManager(new MSSQLDataLayer.BankAccountOverview());

        public struct CustomParam
        {
            public OrderingDTO ordering { get; set; }
            public PaginedDTO paging { get; set; }
        }

        [HttpPost]
        [Route("api/v1/bankAccountOverview")]
        public IHttpActionResult Post()
        {
            var accounts = new List<Entities.DTOs.BankAccountOverview>();
            try
            {
                accounts = this.bankAccountsOverviewManager.Select().ToList();
            }
            catch (Exception e)
            {
                Logger.Error("BankAccountOverview.Get", e.StackTrace);

                return this.InternalServerError(e);
            }

            return this.Ok(accounts);
        }
    }
}