﻿namespace GenericDataLayer.Controllers
{
    using System;
    using System.Net;
    using System.Web.Http;
    using System.Web.Http.Description;
    using Swashbuckle.Swagger.Annotations;
    using GenericDataLayer.Configuration;
    using GenericDataLayer.Managers;
    using GenericDataLayer.Helpers;
    using NLog;
    using System.Collections.Generic;
    using Entities;
    using Entities.DTOs;
    using static Entities.Enums;
    using Entities.Helpers;

    /// <summary>
    /// Blueprints Controller
    /// </summary>
    [RoutePrefix("api/v1/blueprints")]
    public class PlanosController : ApiController
    {
        /// <summary>
        /// logger de nlog 
        /// </summary>
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();


        // entidad sobre la que se esta operando
        private static readonly ulong idEntidad = Convert.ToUInt64(ConfigurationHandler.idEntity());

        /// <summary>
        /// Manager de planos
        /// </summary>
        private readonly PlanosManager planosManager = new PlanosManager();

        /// <summary>
        /// constructor parameterless
        /// </summary>
        public PlanosController()
        { }

        /// <summary>
        /// Get a blueprint by its identifier
        /// </summary>
        /// <param name="id">Unique identifier</param>
        /// <returns>Manager Response</returns>
        [HttpGet]
        [Route("{id}", Name = "PlanosGetById")]
        [ResponseType(typeof(Entities.DTOs.UserDTO))]
        [SwaggerResponse(HttpStatusCode.NotFound)]
        public IHttpActionResult GetByID(ulong id)
        {
            var plano = this.planosManager.Find(id);

            if (plano == null)
            {
                Logger.Debug("No se encontraron datos");

                return this.NotFound();
            }

            var planoDTO = new Entities.DTOs.PlanoDTO();

            foreach (var item in plano)
            {
                planoDTO = item.ToDTO();
            }

            return this.Ok(planoDTO);
        }

        /// <summary>
        /// Get all blueprints based in filtered conditions
        /// </summary>
        /// <param name="idPlano">blueprint id</param>
        /// <param name="ordenDTO">ordering condition</param>
        /// <param name="paginadoDTO">paging condition</param>
        /// <returns>list of blueprints</returns>
        [HttpGet]
        [Route("")]
        [ResponseType(typeof(List<Entities.DTOs.PlanoDTO>))]
        public IHttpActionResult Get([FromUri]ulong? idPlano = null,
                                     [FromUri]OrderingDTO ordenDTO = null,
                                     [FromUri]PaginedDTO paginadoDTO = null)
        {
            // Filtro por Id, User_Name y Perfil
            // Paginación y Ordenamiento(Default Nombre)
            // control paginado
            Paging paginado = null;

            if (paginadoDTO != null && paginadoDTO.IsEnabled)
            {
                paginado = new Paging(paginadoDTO.Page,
                                        paginadoDTO.Rows,
                                        paginadoDTO.TotalRows);
            }

            Ordering orden = null;
            if (ordenDTO?.OrderingRow == null)
            {
                // ordenamiento por defecto
                orden = new Ordering(nameof(Entities.User.Name), OrderingDB.ASC);
            }
            else
            {
                orden = new Ordering(ordenDTO.OrderingRow, ordenDTO.OrdinationSense);
            }

            var planos = this.planosManager.Select(paginado, orden, idPlano);

            var lstDTO = new List<Entities.DTOs.PlanoDTO>();

            foreach(var item in planos)
            {
                lstDTO.Add(item.ToDTO());
            }

            return this.Ok(lstDTO);
        }

        /// <summary>
        /// Add a blueprint
        /// </summary>
        /// <param name="plano">blueprint</param>
        /// <returns>Manager Response with Blueprint added</returns>
        [HttpPost]
        [Route("")]
        [ResponseType(typeof(MgrResponse<Entities.DTOs.PlanoDTO>))]
        [SwaggerResponse(HttpStatusCode.BadRequest, type: typeof(MgrResponse<Entities.DTOs.PlanoDTO>))]
        public IHttpActionResult Post(Entities.DTOs.PlanoDTO plano)
        {
            if (!this.ModelState.IsValid)
            {
                return this.Content(HttpStatusCode.BadRequest,
                    this.ModelState.ToMgrResponse<Entities.DTOs.PlanoDTO>());
            }

            var modelo = plano.ToModel();

            var mgrResponse = this.planosManager.Insert(modelo)
                                .ToDTO<BluePrint, Entities.DTOs.PlanoDTO>();

            if (mgrResponse.Status != RespStatusCodeGeneric.OKInstantImpact &&
                mgrResponse.Status != RespStatusCodeGeneric.OKConfirmationPending)
            {
                return this.Content(HttpStatusCode.BadRequest, mgrResponse);
            }

            return this.CreatedAtRoute("PlanosGetById", new { mgrResponse.Object.ID }, mgrResponse);
        }

        /// <summary>
        /// Modify a blueprint 
        /// </summary>
        /// <param name="plano">blueprint to modify</param>
        /// <returns>Manager Response with blueprint modified</returns>
        [HttpPut]
        [Route("")]
        [ResponseType(typeof(MgrResponse<Entities.DTOs.PlanoDTO>))]
        [SwaggerResponse(HttpStatusCode.BadRequest, type: typeof(MgrResponse<Entities.DTOs.PlanoDTO>))]
        public IHttpActionResult Put(Entities.DTOs.PlanoDTO plano)
        {
            if (!this.ModelState.IsValid)
            {
                return this.Content(HttpStatusCode.BadRequest,
                    this.ModelState.ToMgrResponse<Entities.DTOs.PlanoDTO>());
            }

            var modelo = plano.ToModel();

            var mgrResponse = this.planosManager.Update(modelo).ToDTO<BluePrint, Entities.DTOs.PlanoDTO>();

            if (mgrResponse.Status != RespStatusCodeGeneric.OKInstantImpact &&
                mgrResponse.Status != RespStatusCodeGeneric.OKConfirmationPending)
            {
                return this.Content(HttpStatusCode.BadRequest, mgrResponse);
            }

            return this.Ok(mgrResponse);
        }

        /// <summary>
        /// delete a blueprint
        /// </summary>
        /// <param name="id">blueprint id to delete</param>
        /// <returns>Manager Response with blueprint deleted status</returns>
        [HttpDelete, Route("{id}")]
        [ResponseType(typeof(BluePrintStaus))]
        [SwaggerResponse(HttpStatusCode.NotFound, type: typeof(BluePrintStaus))]
        public IHttpActionResult Delete(ulong id)
        {
            var resp = this.planosManager.Delete(id);

            if (resp == BluePrintStaus.NotFound)
            {
                Logger.Debug("No se encontraron datos");

                return this.NotFound();
            }

            return this.Ok(resp);
        }
    }
}