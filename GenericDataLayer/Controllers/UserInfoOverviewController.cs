﻿namespace GenericDataLayer.Controllers
{
    using GenericDataLayer.Configuration;
    using GenericDataLayer.Helpers;
    using GenericDataLayer.Managers;
    using NLog;
    using Swashbuckle.Swagger.Annotations;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Web.Http;
    using System.Web.Http.Cors;
    using System.Web.Http.Description;
    using static Entities.Enums;

    [RoutePrefix("api/v1/userInfoOverview")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UserInfoOverviewController : ApiController
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private static readonly int idEntity = Convert.ToInt32(ConfigurationHandler.idEntity());
        private readonly IUserInfoOverviewManager userInfoOverviewManager = new UserInfoOverviewManager(new MSSQLDataLayer.UserInfoOverview());

        public struct CustomParam
        {
            public OrderingDTO ordering { get; set; }
            public PaginedDTO paging { get; set; }
        }

        [HttpPost]
        [Route("")]
        [ResponseType(typeof(List<Entities.DTOs.UserInfoOverview>))]
        public IHttpActionResult Post()
        {
            var users = new List<Entities.DTOs.UserInfoOverview>();
            try
            {
                users = this.userInfoOverviewManager.Select().ToList();
            }
            catch (Exception e)
            {
                Logger.Error("UserInfoOverview.Get", e.StackTrace);

                return this.InternalServerError(e);
            }

            return this.Ok(users);
        }

        [HttpDelete]
        [Route("{id}")]
        [ResponseType(typeof(UserInfoStatus))]
        [SwaggerResponse(HttpStatusCode.NotFound, type: typeof(UserInfoStatus))]
        public IHttpActionResult Delete(int id)
        {
            var resp = UserInfoStatus.NotFound;

            try
            {
                resp = this.userInfoOverviewManager.Delete(id);

                if (resp == UserInfoStatus.NotFound)
                {
                    Logger.Debug("Data not found.");

                    return this.StatusCode(HttpStatusCode.NotFound);
                }
            }
            catch (Exception e)
            {
                Logger.Error("UsersController.Delete", e.StackTrace);

                return this.Content(HttpStatusCode.InternalServerError, resp);
            }

            return this.StatusCode(HttpStatusCode.NoContent);
        }
    }
}