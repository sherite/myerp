﻿using System.Data.Entity;

namespace MSSQLDataLayer.Models
{
    /// <summary>
    /// contexto
    /// </summary>
    public class BaseDatos : DbContext // debe heredar de la clase DbContext
    {
        /// <summary>
        /// constructor
        /// </summary>
        public BaseDatos() : base(Properties.Settings.Default.connStr) //<- Pasar la connectionString
        {
        }

        /// <summary>
        /// bancos
        /// </summary>
        public DbSet<Entities.DTOs.BankAccountOverview> BankAccountsOverview { get; set; }

        /// <summary>
        /// usuarios
        /// </summary>
        public DbSet<Entities.DTOs.UserInfoOverview> UserInfoOverview { get; set; }

        /// <summary>
        /// Dynamic menu sidebar
        /// </summary>
        public DbSet<Entities.DTOs.DynamicMenuSideBar> DynamicMenuSideBar { get; set; }

        /// <summary>
        /// menu side bar
        /// </summary>
        public DbSet<Entities.DTOs.Menus> Menus { get; set; }

        /// <summary>
        /// textos
        /// </summary>
        public DbSet<Entities.DTOs.Menus_Texto> MenusTexto { get; set; }




    }
}