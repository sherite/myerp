﻿CREATE PROCEDURE [dbo].[GRUPOS_INSERT] 
	@name as varchar(50),
	@description as varchar(50)
AS

INSERT INTO GROUPS (NAME,DESCRIPTION) VALUES(@name,@description);

SELECT * FROM GROUPS WHERE NAME = @name AND DESCRIPTION = @description;