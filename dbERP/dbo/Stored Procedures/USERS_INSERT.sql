﻿CREATE PROCEDURE [dbo].[USERS_INSERT] 
	@alias as varchar(50),
	@name as varchar(50),
	@lastname as varchar(50)
AS

INSERT INTO USERS (ALIAS, NAME,LASTNAME) VALUES(@alias, @name,@lastname);

SELECT * FROM USERS WHERE ALIAS = @alias AND NAME = @name AND LASTNAME = @lastname;