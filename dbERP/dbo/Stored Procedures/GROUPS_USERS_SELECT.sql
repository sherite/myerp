﻿CREATE PROCEDURE [dbo].[GROUPS_USERS_SELECT] 
	@GroupId int = null
AS
BEGIN

	SELECT USERS.ID, 
		   USERS.ALIAS,
	       USERS.NAME, 
		   USERS.LASTNAME,
		   USERS.STATUS
      FROM USERS 
	       INNER JOIN USERS_GROUPS UG 
		   ON UG.USERID = USERS.ID AND
		   UG.GROUPID = ISNULL(@GroupId, UG.GROUPID)
END