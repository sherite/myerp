﻿CREATE PROCEDURE [dbo].[USERS_GROUPS_INSERT]
	@UserId int = null,
	@GroupId int = null
AS
	INSERT INTO USERS_GROUPS (USERID, GROUPID) VALUES(@UserId, @GroupId);

    RETURN SELECT COUNT(USERID) 
         FROM USERS_GROUPS
        WHERE USERID = @UserId;