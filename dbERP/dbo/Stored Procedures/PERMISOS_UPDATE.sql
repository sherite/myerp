﻿CREATE PROCEDURE [dbo].[PERMISOS_UPDATE]
	@ID  as numeric,
	@Nombre as varchar(50),
	@Descripcion as varchar(50),
	@Estado as numeric(18,0)
AS
	UPDATE PERMISOS SET NOMBRE = @Nombre,
					  DESCRIPCION = @Descripcion,
	                  ID_ESTADO = @Estado
	where ID = @ID;

	RETURN SELECT * FROM PERMISOS WHERE ID=@ID;