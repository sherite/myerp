﻿CREATE PROCEDURE [dbo].[PLANOS_DELETE]
	@idPlano as numeric
AS

	DELETE FROM PLANOS WHERE ID=@idPlano;

	RETURN SELECT COUNT(ID) FROM PLANOS WHERE ID= @idPlano;
