﻿CREATE PROCEDURE [dbo].[ROLES_INSERT] 
	@nombre as varchar(50),
	@descripcion as varchar(50)
AS

INSERT INTO ROLES (NOMBRE,DESCRIPCION) VALUES(@nombre,@descripcion);

SELECT * FROM ROLES WHERE NOMBRE = @nombre AND DESCRIPCION = @descripcion;