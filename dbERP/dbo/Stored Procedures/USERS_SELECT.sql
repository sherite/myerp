﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[USERS_SELECT] 
	-- Add the parameters for the stored procedure here
	@UserId as numeric = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ID, ALIAS, NAME, LASTNAME, [STATUS], 
	(SELECT stuff((
    SELECT ', ' + NAME
    FROM GROUPS
    FOR XML PATH('') ), 1, 2, '')) AS GROUPS
	FROM USERS WHERE ID = ISNULL(@UserId,ID);
END