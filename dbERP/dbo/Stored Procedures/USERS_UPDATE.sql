﻿CREATE PROCEDURE [dbo].[USERS_UPDATE]
	@ID  as int,
    @Alias as varchar(50),
	@Name as varchar(50),
	@LastName as varchar(50),
	@Status as numeric(18,0)
AS
	UPDATE USERS SET ALIAS = @Alias,
					  NAME = @Name,
					  LASTNAME = @LastName,
	                  STATUS = @Status
	OUTPUT INSERTED.*
	where ID = @ID;