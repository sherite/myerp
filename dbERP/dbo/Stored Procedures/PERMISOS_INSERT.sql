﻿CREATE PROCEDURE [dbo].[PERMISOS_INSERT] 
	@nombre as varchar(50),
	@descripcion as varchar(50)
AS

INSERT INTO PERMISOS (NOMBRE,DESCRIPCION) VALUES(@nombre,@descripcion);

SELECT * FROM PERMISOS WHERE NOMBRE = @nombre AND DESCRIPCION = @descripcion;