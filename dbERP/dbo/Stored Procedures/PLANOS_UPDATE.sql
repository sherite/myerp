﻿CREATE PROCEDURE [dbo].[PLANOS_UPDATE]
	@ID  as numeric,
	@Nombre as varchar(50),
	@Estado as numeric(18,0)
AS
	UPDATE PLANOS SET NOMBRE = @Nombre,
	                  ID_ESTADO = @Estado
	where ID = @ID;

	RETURN SELECT * FROM PLANOS WHERE ID=@ID;