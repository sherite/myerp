﻿CREATE PROCEDURE [dbo].[GRUPOS_UPDATE]
	@ID  as numeric,
	@Name as varchar(50),
	@Description as varchar(50),
	@Status as numeric(18,0)
AS

BEGIN
	UPDATE GROUPS SET NAME = @Name,
					  [DESCRIPTION] = @Description,
	                  [STATUS] = @Status
	OUTPUT INSERTED.*
	WHERE ID = @ID
END;