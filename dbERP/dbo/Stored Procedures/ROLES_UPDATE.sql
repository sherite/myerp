﻿CREATE PROCEDURE [dbo].[ROLES_UPDATE]
	@ID  as numeric,
	@Nombre as varchar(50),
	@Descripcion as varchar(50),
	@Estado as numeric(18,0)
AS
	UPDATE ROLES SET NOMBRE = @Nombre,
					  DESCRIPCION = @Descripcion,
	                  ID_ESTADO = @Estado
	where ID = @ID;

	RETURN SELECT * FROM ROLES WHERE ID=@ID;