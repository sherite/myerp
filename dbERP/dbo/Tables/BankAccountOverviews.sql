﻿CREATE TABLE [dbo].[BankAccountOverviews] (
    [AccountID]       NVARCHAR (128) NOT NULL,
    [Name]            NVARCHAR (MAX) NULL,
    [BankGroupID]     NVARCHAR (MAX) NULL,
    [RegistrationNum] NVARCHAR (MAX) NULL,
    [AccountNum]      NVARCHAR (MAX) NULL,
    [LedgerAccount]   NVARCHAR (MAX) NULL,
    [CurrencyCode]    NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_dbo.BankAccountOverviews] PRIMARY KEY CLUSTERED ([AccountID] ASC)
);

