﻿namespace Entities.DTOs
{
    /// <summary>
    /// DTO de Usuario
    /// </summary>
    public class RolDTO
    {
        /// <summary>
        /// Identificador unico del usuario
        /// </summary>
        public int? ID { get; set; }

        /// <summary>
        /// Nombre del usuario
        /// </summary>
        public string Nombre { get; set; }

        /// <summary>
        /// Apellido del usuario
        /// </summary>
        public string Descripcion { get; set; }

        /// <summary>
        /// Estado de usuario
        /// </summary>
        public Entities.Enums.RolStatus ID_Estado { get; set; }
    }
}