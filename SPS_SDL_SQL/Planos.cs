﻿namespace SPS_SDL_SQL
{
    using System.Collections.Generic;
    using Entities;
    using static Entities.Enums;
    
    /// <summary>
    ///  Capa de datos de planos
    /// </summary>
    public class Planos
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public Planos()
        { }

        /// <summary>
        ///  Lista planos filtrados
        /// </summary>
        /// <returns>Lista fltrada</returns>
        public List<BluePrint> Select(object paginado, object orden, ulong? idPlano)
        {
            var miPaginado = (SPS_SDL_SQL.Paging)paginado;

            var retVal = new List<BluePrint>();

            if (General.Connect())
            {
                var cmd = new System.Data.OleDb.OleDbCommand()
                {
                    CommandType = System.Data.CommandType.StoredProcedure,
                    CommandText = "Planos_Select",
                };

                var param1 = new System.Data.OleDb.OleDbParameter()
                {
                    DbType = System.Data.DbType.Int64,
                    Value = idPlano,
                    ParameterName = "@idPlano",
                    IsNullable = true
                };

                cmd.Parameters.Add(param1);

                cmd.Connection = General.Connection;

                var reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    BluePrint plano = null;

                    while (reader.Read())
                    {
                        plano = new BluePrint
                        {
                            ID = int.Parse(reader["ID"].ToString()),
                            Name = reader["Nombre"].ToString(),
                            ID_Estado = (BluePrintStaus)int.Parse(reader["ID_Estado"].ToString())
                        };

                        retVal.Add(plano);
                    }
                }

                reader.Close();

                General.Connection.Close();
                General.Connection = null;
            }

            return retVal;
        }

        /// <summary>
        ///  Busca un plano especifico
        /// </summary>
        /// <param name="id">plano a buscar</param>
        /// <returns>lista con el plano si lo encontro, sino vacia</returns>
        public List<BluePrint> Find(int id)
        {
            var retVal = new List<BluePrint>();

            if (General.Connect())
            {
                General.Connection.Close();
            }

            return retVal;
        }

        /// <summary>
        /// Actualiza un plano
        /// </summary>
        /// <param name="base64">encoding de entrada</param>
        /// <returns>plano actualizado</returns>
        public BluePrint Update(string base64)
        {
            var param = System.Convert.FromBase64String(base64);
            var json2bytes = System.Text.Encoding.UTF8.GetString(param);
            var plano = Newtonsoft.Json.JsonConvert.DeserializeObject<BluePrint>(json2bytes);

            var retVal = new BluePrint();

            if (General.Connect())
            {
                var cmd = new System.Data.OleDb.OleDbCommand()
                {
                    CommandType = System.Data.CommandType.StoredProcedure,
                    CommandText = "Planos_Update",
                };

                cmd.Parameters.Add(
                    new System.Data.OleDb.OleDbParameter()
                    {
                        DbType = System.Data.DbType.Int32,
                        Value = plano.ID,
                        ParameterName = "@ID",
                        IsNullable = false

                    });

                cmd.Parameters.Add(
                    new System.Data.OleDb.OleDbParameter()
                    {
                        DbType = System.Data.DbType.String,
                        Value = plano.Name,
                        ParameterName = "@Nombre",
                        IsNullable = false

                    });

                cmd.Parameters.Add(
                    new System.Data.OleDb.OleDbParameter()
                    {
                        DbType = System.Data.DbType.Int32,
                        Value = (int)plano.ID_Estado,
                        ParameterName = "@Estado",
                        IsNullable = true

                    });


                cmd.Connection = General.Connection;

                var reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    BluePrint miPlano = null;

                    while (reader.Read())
                    {
                        miPlano = new BluePrint
                        {
                            ID = int.Parse(reader["ID"].ToString()),
                            Name = reader["Nombre"].ToString(),
                            ID_Estado = (BluePrintStaus)int.Parse(reader["ID_Estado"].ToString())
                        };

                        retVal = miPlano;
                    }
                }

                reader.Close();


                General.Connection.Close();
            }

            return retVal;
        }

        /// <summary>
        /// Borra un plano especifico
        /// </summary>
        /// <param name="id"></param>
        /// <returns>resultado del borrado</returns>
        public BluePrintStaus Delete(int id)
        {
            var retval = BluePrintStaus.NotFound;

            if (General.Connect())
            {
                var cmd = new System.Data.OleDb.OleDbCommand()
                {
                    CommandType = System.Data.CommandType.StoredProcedure,
                    CommandText = "Planos_Delete",
                };

                cmd.Parameters.Add(
                    new System.Data.OleDb.OleDbParameter()
                    {
                        DbType = System.Data.DbType.Int32,
                        Value = id,
                        ParameterName = "@idPlano",
                        IsNullable = false

                    });

                cmd.Connection = General.Connection;

                var reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                    }
                }

                reader.Close();

                General.Connection.Close();
                General.Connection = null;
            }

            return retval;

        }

        /// <summary>
        /// Inserta un plano
        /// </summary>
        /// <param name="base64">datos a insertar</param>
        /// <returns>plano insertado</returns>
        public BluePrint Insert(string base64)
        {
            var param = System.Convert.FromBase64String(base64);
            var json2bytes = System.Text.Encoding.UTF8.GetString(param);
            var plano = Newtonsoft.Json.JsonConvert.DeserializeObject<BluePrint>(json2bytes);

            var retVal = new BluePrint();

            if (General.Connect())
            {
                var cmd = new System.Data.OleDb.OleDbCommand()
                {
                    CommandType = System.Data.CommandType.StoredProcedure,
                    CommandText = "Planos_Insert",
                };

                var param1 = new System.Data.OleDb.OleDbParameter()
                {
                    DbType = System.Data.DbType.String,
                    Value = plano.Name,
                    ParameterName = "@nombre",
                    IsNullable = false

                };

                cmd.Parameters.Add(param1);

               cmd.Connection = General.Connection;

                var reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    BluePrint miPlano = null;

                    while (reader.Read())
                    {
                        miPlano = new BluePrint
                        {
                            ID = int.Parse(reader["ID"].ToString()),
                            Name = reader["Nombre"].ToString(),
                            ID_Estado = (BluePrintStaus)int.Parse(reader["ID_Estado"].ToString())
                        };

                        retVal = miPlano;
                    }
                }

                reader.Close();


                General.Connection.Close();
                General.Connection = null;
            }

            return retVal;
        }
    }
}
