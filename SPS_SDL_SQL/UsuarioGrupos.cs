﻿namespace SPS_SDL_SQL
{
    using Entities;
    using Entities.DTOs;
    using System;
    using System.Collections.Generic;
    using static Entities.Enums;

    public class UserGroups
    {
        public UserGroups() { }

        public List<Group> Select(int userId)
        {
            var retVal = new List<Group>();

            try
            {
                if (General.Connect())
                {
                    var cmd = new System.Data.OleDb.OleDbCommand()
                    {
                        CommandType = System.Data.CommandType.StoredProcedure,
                        CommandText = "Users_Groups_Select",
                    };

                    var cmdParam = new System.Data.OleDb.OleDbParameter()
                    {
                        DbType = System.Data.DbType.Int64,
                        Value = userId,
                        ParameterName = "@UserId",
                        IsNullable = true
                    };

                    cmd.Parameters.Add(cmdParam);

                    cmd.Connection = General.Connection;

                    var reader = cmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            var group = new Group
                            {
                                ID = int.Parse(reader["ID"].ToString()),
                                Name = reader["Name"].ToString(),
                                Description = reader["Description"].ToString(),
                                Status = (GroupStatus)int.Parse(reader["Status"].ToString())
                            };

                            retVal.Add(group);
                        }
                    }

                    reader.Close();

                    General.Connection.Close();
                    General.Connection = null;
                }
            }
            catch
            {
                throw;
            }

            return retVal;
        }

        public IList<UsuarioGetRequestDTO> SelectUsers(int groupId)
        {
            var retVal = new List<UsuarioGetRequestDTO>();

            try
            {
                if (General.Connect())
                {
                    var cmd = new System.Data.OleDb.OleDbCommand()
                    {
                        CommandType = System.Data.CommandType.StoredProcedure,
                        CommandText = "Groups_Users_Select",
                    };

                    var cmdParam = new System.Data.OleDb.OleDbParameter()
                    {
                        DbType = System.Data.DbType.Int64,
                        Value = groupId,
                        ParameterName = "@GroupId",
                        IsNullable = true
                    };

                    cmd.Parameters.Add(cmdParam);

                    cmd.Connection = General.Connection;

                    var reader = cmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            var user = new UsuarioGetRequestDTO()
                            {
                                ID = int.Parse(reader["ID"].ToString()),
                                Name = reader["Name"].ToString(),
                                LastName = reader["LastName"].ToString(),
                                Alias = reader["Alias"].ToString(),
                                Status = (UserStatus)(Convert.ToInt32((reader["Status"].ToString())))
                            };

                            retVal.Add(user);
                        }
                    }

                    reader.Close();

                    General.Connection.Close();
                    General.Connection = null;
                }
            }
            catch
            {

                throw;
            }

            return retVal;
        }

        public List<Group> Find(int id)
        {
            return Select(id);
        }

        public int Update(string base64)
        {
            return 0;
        }

        public void DeleteUsers(int? idGrupo)
        {
            if (General.Connect())
            {
                var cmd = new System.Data.OleDb.OleDbCommand()
                {
                    CommandType = System.Data.CommandType.StoredProcedure,
                    CommandText = "Groups_Users_Delete",
                };

                cmd.Parameters.Add(
                    new System.Data.OleDb.OleDbParameter()
                    {
                        DbType = System.Data.DbType.Int32,
                        Value = idGrupo,
                        ParameterName = "@groupId",
                        IsNullable = true

                    });

                cmd.Connection = General.Connection;

                cmd.ExecuteNonQuery();

                General.Connection.Close();
                General.Connection = null;
            }
        }

        public void Delete(int? idUsuario)
        {
            if (General.Connect())
            {
                var cmd = new System.Data.OleDb.OleDbCommand()
                {
                    CommandType = System.Data.CommandType.StoredProcedure,
                    CommandText = "Users_Groups_Delete",
                };

                cmd.Parameters.Add(
                    new System.Data.OleDb.OleDbParameter()
                    {
                        DbType = System.Data.DbType.Int32,
                        Value = idUsuario.Value,
                        ParameterName = "@userId",
                        IsNullable = true

                    });

                cmd.Connection = General.Connection;

                var result = cmd.ExecuteNonQuery();

                General.Connection.Close();
                General.Connection = null;
            }
        }

        public void InsertFromNewUser(int userId, int groupId, System.Data.OleDb.OleDbCommand cmd)
        {
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "Users_Groups_Insert";

            cmd.Parameters.Clear();

            cmd.Parameters.Add(new System.Data.OleDb.OleDbParameter()
            {
                DbType = System.Data.DbType.Int32,
                Value = userId,
                ParameterName = "@userId",
                IsNullable = false
            });

            cmd.Parameters.Add(new System.Data.OleDb.OleDbParameter()
            {
                DbType = System.Data.DbType.Int32,
                Value = groupId,
                ParameterName = "@groupId",
                IsNullable = false
            });

            var  reader = cmd.ExecuteReader();

            reader.Close();
        }

        public void Insert(int userId, int groupId)
        {
            if (General.Connect())
            {
                var cmd = new System.Data.OleDb.OleDbCommand()
                {
                    CommandType = System.Data.CommandType.StoredProcedure,
                    CommandText = "Users_Groups_Insert",
                };

                var param = new System.Data.OleDb.OleDbParameter()
                {
                    DbType = System.Data.DbType.Int32,
                    Value = userId,
                    ParameterName = "@userId",
                    IsNullable = false
                };

                cmd.Parameters.Add(param);

                param = new System.Data.OleDb.OleDbParameter()
                {
                    DbType = System.Data.DbType.Int32,
                    Value = groupId,
                    ParameterName = "@groupId",
                    IsNullable = false
                };

                cmd.Parameters.Add(param);

                cmd.Connection = General.Connection;

                cmd.ExecuteNonQuery();

                General.Connection.Close();
                General.Connection = null;
            }
        }
    }
}